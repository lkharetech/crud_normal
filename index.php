<?php
if (isset($_GET['id']) && $_GET['id'] != '') {
    $userid = $_GET['id'];
    include('dbconnection.php');
    $sql = "SELECT * FROM `newcrud` WHERE `userid` = '$userid'";
    $result = mysqli_query($con, $sql);

    $data = mysqli_fetch_array($result);
}

?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <title>NORMAL CRUD</title>
</head>

<body>
    <!-- Main front page -->
    <div class="container">
        <div>
            <br>
            <h2> Normal CRUD Sign-Up Form</h2>
        </div>
        <br>
        <form action="insert.php" id="signupForm" name="mainform" method="POST" enctype="multipart/form-data">
            <div class="">
                <label for="fullname">Fullname :</label>
                <input type="text" id="fullname" name="fullname" placeholder="Enter your fullname" value="<?php if (isset($data)) {
                                                                                                                echo $data['fullname'];
                                                                                                            } ?>">
            </div>
            <br>
            <div>
                <label for="mobile">Mobile No. :</label>
                <input type="text" id="mobile" name="mobile" placeholder="Enter your mobile no." value="<?php if (isset($data)) {
                                                                                                            echo $data['mobile'];
                                                                                                        } ?>">
            </div>
            <br>
            <div>
                <label for="email">Email Id :</label>
                <input type="email" id="email" name="email" placeholder="Enter your email id" value="<?php if (isset($data)) {
                                                                                                            echo $data['email'];
                                                                                                        } ?>">
            </div>
            <br>
            <div>
                <label for="password">Password :</label>
                <input type="password" id="password" name="password" placeholder="Enter your password" onkeyup="checkPasswordStrength();" value="<?php if (isset($data)) {
                                                                                                                                                        echo $data['password'];
                                                                                                                                                    } ?>">
                <div id="password-strength-status"></div>
            </div>
            <br>
            <div>
                <label for="image">Upload file :</label>
                <input type="file" id="image" name="image" onchange="document.getElementById('image_preview').src = window.URL.createObjectURL(this.files[0])">
                <img id="image_preview" alt="image" width="100" height="100" src="<?php if (isset($data)) {
                                                                                        echo $data['image'];
                                                                                    } ?>"/>
            </div>
            <br>
            <input type="hidden" name="userid" value="<?php if (isset($data)) {
                                                            echo $data['userid'];
                                                        } ?>">
                <div>
                    <button type="submit" name="submit">Submit</button>
                </div>
        </form>



    </div>

    <!-- jquery validation links -->

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.min.js"></script>

    <!-- jquery validation -->
    <script>
        $("#signupForm").validate({
            rules: {
                fullname: {
                    required: true,
                },
                mobile: {
                    required: true,
                    minlength: 10,
                    maxlength: 10,
                    number: true
                },
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true
                }

            },
            messages: {
                fullname: {
                    required: " Please provide a valid fullname !!"
                },
                mobile: {
                    required: " Please enter your mobile no. !!",
                    minlength: " Please enter mobile no. of 10 digits !!",
                    maxlength: " Mobile no. cannot be more than 10 digits !!",
                    number: "Please enter a valid number !!"
                },
                email: {
                    required: " Please enter your email id !!",
                    email: "Please enter a valid email - id !!"
                },
                password: {
                    required: " Please enter your password !!",
                }
            }
        });
    </script>

    <!-- Shows password strength -->
    <script>
        function checkPasswordStrength() {
            var number = /([0-9])/;
            var alphabets = /([a-zA-Z])/;
            var special_characters = /([~,!,@,#,$,%,^,&,*,-,_,+,=,?,>,<])/;
            var password = $('#password').val().trim();
            if (password.length < 6) {
                $('#password-strength-status').removeClass();
                $('#password-strength-status').addClass('weak-password');
                $('#password-strength-status').html("Weak (should be atleast 6 characters.)");
            } else {
                if (password.match(number) && password.match(alphabets) && password.match(special_characters)) {
                    $('#password-strength-status').removeClass();
                    $('#password-strength-status').addClass('strong-password');
                    $('#password-strength-status').html("Strong");
                } else {
                    $('#password-strength-status').removeClass();
                    $('#password-strength-status').addClass('medium-password');
                    $('#password-strength-status').html("Medium (should include alphabets, numbers and special characters.)");
                }
            }
        }
    </script>



    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script> -->

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    -->
</body>

</html>