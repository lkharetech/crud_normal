<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>DATA VIEW PAGE</title>
</head>

<body>
    <div class="container">
        <br>
        <div>
            <a href="index.php">ADD NEW DATA</a>
        </div>
        <?php
        include("dbconnection.php");

        $sql = "SELECT * FROM `newcrud` ORDER BY userId DESC";

        $result = mysqli_query($con, $sql);
        if (mysqli_num_rows($result) > 0) {   ?>
            <div class="text-center">
                <h1>FORM DATA</h1>
            </div>
            <table class="table">
                <thead>
                    <tr>
                        <th>S. No.</th>
                        <th>Fullname</th>
                        <th>Mobile</th>
                        <th>Email</th>
                        <th>Password</th>
                        <th>Image</th>
                        <th>Creation Date & Time</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $counter = 1;
                    
                    while ($data = mysqli_fetch_array($result, MYSQLI_ASSOC)) { ?>
                        
                        <tr>
                            <td><?php echo $counter++; ?></td>
                            <td><?php echo $data['fullname']; ?></td>
                            <td><?php echo $data['mobile']; ?></td>
                            <td><?php echo $data['email']; ?></td>
                            <td><?php echo $data['password']; ?></td>
                            <td><img src="<?php echo $data['image']; ?>" width="100" height="100"> </td>
                            <td><?php echo $data['creationtime']; ?></td>
                            
                            <td>
                                <form action="delete.php" method="POST">
                                    <input type="hidden" name="userid" value="<?php echo $data['userid']; ?>">
                                    <button onclick="return confirm('Are you sure you want to delete the data ?')" type="submit">DELETE</button>
                                </form>
                                <a href="index.php?id=<?php echo $data['userid'] ?>">EDIT</a>
                            </td>
                            

                        </tr>

                    <?php } ?>
                </tbody>
            </table>

        <?php } else {
            echo "Data Not Found";
        } ?>




    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    -->
</body>

</html>